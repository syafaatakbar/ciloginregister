<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function index()
    {
        //ambil data user dari session auth _login email, role id, yg email dari table user dari session set_userdata (auth) yg emailnya = email session login
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        // echo 'halo ' . $data['user']['name']; //ambil datanya khusus dicontroller 

        $data['title'] = 'Dashboard';

        $this->load->view('templates/header.php', $data);
        $this->load->view('templates/sidebar.php', $data);
        $this->load->view('templates/topbar.php', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer.php', $data);
    }
}
