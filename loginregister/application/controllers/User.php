<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function index()
    {
        //ambil data user dari session auth _login email, role id, yg email dari table user dari session set_userdata (auth) yg emailnya = email session login
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        // echo 'halo ' . $data['user']['name']; //ambil datanya khusus dicontroller 

        $data['title'] = 'My Profile';

        $this->load->view('templates/header.php', $data);
        $this->load->view('templates/sidebar.php', $data);
        $this->load->view('templates/topbar.php', $data);
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer.php', $data);
    }
}
